#Conway's Game of Life 


###SETUP

1. git clone git@github.com:vjdhama/gameoflife2.git
2. cd gameoflife
3. bundle install

##BUILD

1. bundle exec rake
2. bundle exec rspec [SPECFILE_PATH:LINE]

##LAUNCH

