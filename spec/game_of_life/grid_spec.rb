require 'spec_helper'

module GameOfLife
  describe Grid do
    it "should generate next generation" do
      grid = Grid.new(3, 4)
      live_locations = [[0, 0], [1, 0], [2,0], [1, 1]]
      grid.feed_live_locations(live_locations)
      grid.next_generation
      expect(grid.live_count).to eq(6)
    end
  end
end
