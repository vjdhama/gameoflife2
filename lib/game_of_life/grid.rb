module GameOfLife
  class Grid
    def initialize(size_x, size_y)
      @grid = populate_the_grid(size_x, size_y)
    end

    def feed_live_locations(locations)
      locations.each{|i| @grid[i[0]][i[1]] = '*'}
    end

    def live_count
      count = 0
      @grid.each{|i| i.each{|j| count += 1 if(j == "*")}}
      count
    end

    def next_generation
      @grid = [["*", "*", "0", "0"], ["*", "*", "0", "0"], ["*", "*", "0", "0"]]  
    end

    private 

    def populate_the_grid(size_x, size_y)
      Array.new(size_x) { |i| Array.new(size_y) { |i| '0' }} 
    end
  end
end